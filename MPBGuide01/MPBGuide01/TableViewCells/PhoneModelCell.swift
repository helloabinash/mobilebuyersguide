//
//  PhoneModelCell.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import UIKit

class PhoneModelCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgViewPhone: UIImageView!
    @IBOutlet weak var btnFavourite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func load(model: PhoneInfoModel, index: Int, hideFavourite: Bool = false) {
        self.lblTitle.text = model.name
        self.lblDescription.text = model.description
        self.lblPrice.text = "Price: $\(model.price)"
        self.lblRating.text = "Rating: \(model.rating)"
        self.imgViewPhone?.load(strURL: model.thumbImageURL)
        self.btnFavourite.isSelected = model.favourite
        self.btnFavourite.tag = index
        self.btnFavourite.isHidden = hideFavourite
    }

}
