//
//  AllModelsViewModel.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

class AllModelsViewModel {
    var phoneModels = [PhoneInfoModel]()
    init(model: [PhoneInfoModel]? = nil) {
        if let inputModel = model {
            phoneModels = inputModel
        }
    }
    
    func sort(by option: MobilePhoneSort) {
        switch option {
        case .priceAscending:
            phoneModels = phoneModels.sorted(by: {$0.price < $1.price})
        case .priceDescending:
            phoneModels = phoneModels.sorted(by: {$0.price > $1.price})
        case .rating:
            phoneModels = phoneModels.sorted(by: {$0.rating > $1.rating})
        }
    }
}

extension AllModelsViewModel {
    func fetchPhoneModels(completion: @escaping (Result<[PhoneInfoModel], Error>) -> Void) {
        NetworkManager().loadPhoneData { [weak self] result in
            guard let unwrappedSelf = self else { return }
            switch result {
            case .failure(let error):
                print ("failure", error)
            case .success(let models) :
                if let unwrappedModels = models {
                    unwrappedSelf.phoneModels = unwrappedModels
                    completion(.success(unwrappedModels))
                }
            }
        }
    }
}
