//
//  PhoneDetailViewModel.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

class PhoneDetailViewModel {
    var phone: PhoneInfoModel!
    var phoneImages = [PhoneImageModel]()
    init(phone: PhoneInfoModel, images: [PhoneImageModel]? = nil) {
        self.phone = phone
        if let images = images {
            phoneImages = images
        }
    }
}

extension PhoneDetailViewModel {
    func fetchPhoneImages(completion: @escaping (Result<[PhoneImageModel], Error>) -> Void) {
        NetworkManager().loadPhoneImages(id: phone.id) { [weak self] result in
            guard let unwrappedSelf = self else { return }
            switch result {
            case .failure(let error):
                print ("failure", error)
            case .success(let models) :
                if let unwrappedModels = models {
                    unwrappedSelf.phoneImages = unwrappedModels
                    completion(.success(unwrappedModels))
                }
            }
        }
    }
}
