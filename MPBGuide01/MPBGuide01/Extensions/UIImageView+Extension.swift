//
//  UIImageView+Extension.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation
import UIKit

var imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    func load(strURL: String) {
        if let image = imageCache.object(forKey: strURL as NSString) as? UIImage {
            self.image = image
            return
        }
        guard let url = URL(string: strURL) else { return }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    imageCache.setObject(image, forKey: strURL as NSString)
                    self?.image = image
                }
            }
        }
    }
}
