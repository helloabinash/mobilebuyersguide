//
//  Constants.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

// MARK:- API
let endpoint = "https://scb-test-mobile.herokuapp.com/api/mobiles/"


// MARK:- Notifications
let kSortNotification = NSNotification.Name(rawValue: "SortOptionChangeNotification")
let kSortKey = "sortKey"

// MARK:- Segues
let kShowDetailSegue = "showModelDetailSegue"
