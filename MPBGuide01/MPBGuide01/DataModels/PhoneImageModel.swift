//
//  PhoneImageModel.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

struct PhoneImageModel: Codable {
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case mobileId = "mobile_id"
        case url = "url"
    }
    var id: Int
    var mobileId: Int
    var url: String
}

