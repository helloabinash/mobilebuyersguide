//
//  PhoneDataModel.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

struct PhoneInfoModel: Codable {
    
    private enum CodingKeys: String, CodingKey {
        case thumbImageURL = "thumbImageURL"
        case rating = "rating"
        case id = "id"
        case name = "name"
        case description = "description"
        case price = "price"
        case brand = "brand"
        case favourite = "favourite"
    }
    
    var thumbImageURL: String
    var rating: Double
    var id: Int
    var name: String
    var description: String
    var price: Double
    var brand: String
    var favourite: Bool
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.thumbImageURL = try values.decode(String.self, forKey: .thumbImageURL)
        self.rating = try values.decode(Double.self, forKey: .rating)
        self.id = try values.decode(Int.self, forKey: .id)
        self.name = try values.decode(String.self, forKey: .name)
        self.description = try values.decode(String.self, forKey: .description)
        self.price = try values.decode(Double.self, forKey: .price)
        self.brand = try values.decode(String.self, forKey: .brand)
        self.favourite = try values.decodeIfPresent(Bool.self, forKey: .favourite) ?? false
    }
    
}
