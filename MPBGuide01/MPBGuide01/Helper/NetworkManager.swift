//
//  NetworkManager.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import Foundation

enum NetworkResult {
    case success(Data)
    case failure(Error?)
}

class NetworkManager {
    func loadData(from url: URL,
                  completionHandler: @escaping (NetworkResult) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            let result = data.map(NetworkResult.success) ?? .failure(error)
            completionHandler(result)
        }
        task.resume()
    }
    
    func loadPhoneData(completionHandler: @escaping (Result<[PhoneInfoModel]?, Error>) -> Void) {
        let url = URL(string: endpoint)!
        
        loadData(from: url) { result in
            switch result {
            case .success(let data):
                let phoneData = self.decode(data: data, to: [PhoneInfoModel].self)
                completionHandler(.success(phoneData))
            case .failure(let err):
                completionHandler(.failure(err!))
                print(err?.localizedDescription ?? "Error Occurred")
            }
            
        }
    }
    
    func loadPhoneImages(id: Int, completionHandler: @escaping (Result<[PhoneImageModel]?, Error>) -> Void) {
        let url = URL(string: "\(endpoint)\(id)/images/")!
        
        loadData(from: url) { result in
            switch result {
            case .success(let data):
                let phoneImgData = self.decode(data: data, to: [PhoneImageModel].self)
                completionHandler(.success(phoneImgData))
            case .failure(let err):
                completionHandler(.failure(err!))
                print(err?.localizedDescription ?? "Error Occurred")
            }
            
        }
    }
    
    func decode<T: Decodable>(data: Data, to type: T.Type) -> T? {
        do {
            let jsonDecoder = JSONDecoder()
            let decodedResponse = try jsonDecoder.decode(type, from: data)
            return decodedResponse
        } catch {
            print(error)
            return nil
        }
    }
    
}
