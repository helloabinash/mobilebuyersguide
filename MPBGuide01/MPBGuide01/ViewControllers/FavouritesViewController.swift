//
//  FavouritesViewController.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import UIKit

class FavouritesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: AllModelsViewModel!
    var favouriteModels = [PhoneInfoModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(sortOptionDidChange(notification:)), name: kSortNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTableView()
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self, name: kSortNotification, object: nil)
    }
    
    private func updateTableView() {
        favouriteModels = viewModel.phoneModels.filter({$0.favourite == true})
        tableView.reloadData()
    }
    
    @objc func sortOptionDidChange(notification: Notification) {
        if let sortOption = notification.userInfo![kSortKey] as? MobilePhoneSort {
            viewModel.sort(by: sortOption)
            self.updateTableView()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kShowDetailSegue, let phone = sender as? PhoneInfoModel {
            let detailVC = segue.destination as? PhoneDetailViewController
            detailVC?.viewModel = PhoneDetailViewModel(phone: phone)
        }
    }
}

extension FavouritesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneModelCell", for: indexPath) as! PhoneModelCell
        cell.load(model: favouriteModels[indexPath.row], index: indexPath.row, hideFavourite: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let row = viewModel.phoneModels.firstIndex(where: {$0.id == favouriteModels[indexPath.row].id}) {
                viewModel.phoneModels[row].favourite = false
            }
            favouriteModels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: kShowDetailSegue, sender: viewModel.phoneModels[indexPath.row])
    }
}
