//
//  PhoneDetailViewController.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import UIKit

class PhoneDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textViewDetail: UITextView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    var viewModel: PhoneDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchPhoneImages {[weak self] result in
            DispatchQueue.main.async {
                self?.updateView()
            }
        }
    }
    
    private func updateView() {
        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        for index in 0..<viewModel.phoneImages.count {
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            
            let imageView = UIImageView(frame: frame)
            imageView.contentMode = .scaleAspectFit
            imageView.load(strURL: viewModel.phoneImages[index].url)
            self.scrollView.addSubview(imageView)
        }
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(viewModel.phoneImages.count), height: scrollView.frame.size.height)
        self.title = viewModel.phone.name
        self.textViewDetail.text = viewModel.phone.description
        self.lblPrice.text = "Price: $\(viewModel.phone.price)"
        self.lblRating.text = "Rating: \(viewModel.phone.rating)"
        pageControl.numberOfPages = viewModel.phoneImages.count
    }

}

extension PhoneDetailViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/scrollView.frame.size.width
        pageControl.currentPage = Int(page)
    }
}
