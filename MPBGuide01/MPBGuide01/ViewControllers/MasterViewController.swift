//
//  MasterViewController.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import UIKit

enum MobilePhoneSort : String, CaseIterable {
    case priceAscending = "Price low to high"
    case priceDescending = "Price high to low"
    case rating = "Rating"
}

class MasterViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnAllModels: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
    
    var viewModel = AllModelsViewModel()
    
    lazy var allModelsVC: AllModelsViewController = {
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        var vc = storyboard?.instantiateViewController(withIdentifier: "AllModelsViewController") as! AllModelsViewController
        vc.viewModel = viewModel
        self.addChild(vc)
        return vc
    }()
    lazy var favouriteVC: FavouritesViewController = {
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        var vc = storyboard?.instantiateViewController(withIdentifier: "FavouritesViewController") as! FavouritesViewController
        vc.viewModel = viewModel
        self.addChild(vc)
        return vc
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleSegmentButtons(btnAllModels)
    }
    
    @IBAction func handleSegmentButtons(_ sender: UIButton) {
        if sender == btnAllModels {
            btnFavourite.isSelected = false
            btnAllModels.isSelected = true
            removeChildVC(child: favouriteVC)
            addChildVC(child: allModelsVC)
        } else {
            btnFavourite.isSelected = true
            btnAllModels.isSelected = false
            removeChildVC(child: allModelsVC)
            addChildVC(child: favouriteVC)
        }
    }
    
    private func addChildVC(child: UIViewController) {
        addChild(child)
        containerView.addSubview(child.view)
        child.view.frame = containerView.bounds
        child.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        child.didMove(toParent: self)
    }
    private func removeChildVC(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    @IBAction func handleSorting(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: MobilePhoneSort.priceAscending.rawValue, style: .default, handler: { action in
            NotificationCenter.default.post(name: kSortNotification , object: nil, userInfo: [kSortKey : MobilePhoneSort.priceAscending])
        }))
        alert.addAction(UIAlertAction(title: MobilePhoneSort.priceDescending.rawValue, style: .default, handler: { action in
            NotificationCenter.default.post(name: kSortNotification , object: nil, userInfo: [kSortKey : MobilePhoneSort.priceDescending])
        }))
        alert.addAction(UIAlertAction(title: MobilePhoneSort.rating.rawValue, style: .default, handler: { action in
            NotificationCenter.default.post(name: kSortNotification , object: nil, userInfo: [kSortKey : MobilePhoneSort.rating])
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}
