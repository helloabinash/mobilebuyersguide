//
//  AllModelsViewController.swift
//  MPBGuide01
//
//  Created by abinash das on 08/08/21.
//

import UIKit

class AllModelsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel: AllModelsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchPhoneModels {[weak self] result in
            DispatchQueue.main.async {
                self?.updateTableView()
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(sortOptionDidChange(notification:)), name: kSortNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTableView()
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self, name: kSortNotification, object: nil)
    }
    
    private func updateTableView() {
        tableView.reloadData()
    }
    
    @IBAction func handleFavouriteAction(_ sender: UIButton) {
        sender.isSelected = true
        viewModel.phoneModels[sender.tag].favourite = true
    }
    
    @objc func sortOptionDidChange(notification: Notification) {
        if let sortOption = notification.userInfo![kSortKey] as? MobilePhoneSort {
            viewModel.sort(by: sortOption)
            self.updateTableView()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kShowDetailSegue, let phone = sender as? PhoneInfoModel {
            let detailVC = segue.destination as? PhoneDetailViewController
            detailVC?.viewModel = PhoneDetailViewModel(phone: phone)
        }
    }
}

extension AllModelsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.phoneModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneModelCell", for: indexPath) as! PhoneModelCell
        cell.load(model: viewModel.phoneModels[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: kShowDetailSegue, sender: viewModel.phoneModels[indexPath.row])
    }
}
